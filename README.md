# pho-earth

Modification of the great Pho-Myrtus theme by Guilmour (https://www.gnome-look.org/p/1240355/).
It has warmer colours, inspired by our beautiful Mother Nature.
Modifications go further than just the palette.